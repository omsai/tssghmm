# TSS GHMM

Reproduce the hidden Markov Model from Core et al 2014
for finding transcription start sites (TSSs)
using an HMM framework that allows tying states together.

The intention is to publish the model as an R Bioconductor package,
where using C is preferred over C++ by the Bioconductor maintenance
team for which the [GHMM C library](http://ghmm.org) appears to be a
well-written and mature framework.  GHMM has been used in over a dozen
publications since 2000.

# TSS model publication

[Figure
3](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4254663/figure/F3/),
[Supplementary Figure
2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4254663/bin/NIHMS635899-supplement-1.pdf),
and
[Methods](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4254663/#S8title)
in:

Core, L. J. et al. Analysis of nascent RNA identifies a unified
architecture of initiation regions at mammalian promoters and
enhancers. Nature Genetics 46, 1311–1320 (2014)
[doi:10.1038/ng.3142](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4254663/)

# Build using the GHMM system library

GHMM is a mature library available in most GNU/Linux distributions.
On Debian:

```console
$ sudo apt install libghmm-dev
```

Clone this repository and build using meson:

```console
$ mkdir -p ~/src
$ cd ~/src
$ git clone https://gitlab.com/omsai/tssghmm.git
...
$ cd tssghmm/
$ meson --pkg-config-path $PWD build
The Meson build system
Version: 0.58.1
Source dir: /home/omsai/src/tssghmm
Build dir: /home/omsai/src/tssghmm/build
Build type: native build
Project name: tssghmm
Project version: 0.1-dev
C compiler for the host machine: cc (gcc 8.3.0 "cc (Debian 8.3.0-6) 8.3.0")
C linker for the host machine: cc ld.bfd 2.31.1
Host machine cpu family: x86_64
Host machine cpu: x86_64
Found pkg-config: /usr/bin/pkg-config (0.29)
Run-time dependency ghmm found: YES 0.9-rc3
Library m found: YES
Build targets in project: 5

Found ninja-1.10.0.git.kitware.jobserver-1 at /home/omsai/.local/bin/ninja
$ ninja -v -C build/
ninja: Entering directory `build/'
[1/12] cc -Itest-stable.p -I. -I.. -I/usr/include/libxml2 -fdiagnostics-color=always -D_FILE_OFFSET_BITS=64 -Wall -Winvalid-pch -std=c99 -g -MD -MQ test-stable.p/test-stable.c.o -MF test-stable.p/test-stable.c.o.d -o test-stable.p/test-stable.c.o -c ../test-stable.c
[2/12] cc -Ilibtsshmm.so.p -I. -I.. -I/usr/include/libxml2 -fdiagnostics-color=always -D_FILE_OFFSET_BITS=64 -Wall -Winvalid-pch -std=c99 -g -fPIC -MD -MQ libtsshmm.so.p/models.c.o -MF libtsshmm.so.p/models.c.o.d -o libtsshmm.so.p/models.c.o -c ../models.c
[3/12] cc -Itest-monotonic.p -I. -I.. -I/usr/include/libxml2 -fdiagnostics-color=always -D_FILE_OFFSET_BITS=64 -Wall -Winvalid-pch -std=c99 -g -MD -MQ test-monotonic.p/test-monotonic.c.o -MF test-monotonic.p/test-monotonic.c.o.d -o test-monotonic.p/test-monotonic.c.o -c ../test-monotonic.c
[4/12] cc -Itest-converge.p -I. -I.. -I/usr/include/libxml2 -fdiagnostics-color=always -D_FILE_OFFSET_BITS=64 -Wall -Winvalid-pch -std=c99 -g -MD -MQ test-converge.p/test-converge.c.o -MF test-converge.p/test-converge.c.o.d -o test-converge.p/test-converge.c.o -c ../test-converge.c
[5/12] cc -Itest-init.p -I. -I.. -I/usr/include/libxml2 -fdiagnostics-color=always -D_FILE_OFFSET_BITS=64 -Wall -Winvalid-pch -std=c99 -g -MD -MQ test-init.p/test-init.c.o -MF test-init.p/test-init.c.o.d -o test-init.p/test-init.c.o -c ../test-init.c
[6/12] cc -Ilibtsshmm.so.p -I. -I.. -I/usr/include/libxml2 -fdiagnostics-color=always -D_FILE_OFFSET_BITS=64 -Wall -Winvalid-pch -std=c99 -g -fPIC -MD -MQ libtsshmm.so.p/compare.c.o -MF libtsshmm.so.p/compare.c.o.d -o libtsshmm.so.p/compare.c.o -c ../compare.c
[7/12] cc  -o libtsshmm.so libtsshmm.so.p/models.c.o libtsshmm.so.p/compare.c.o -Wl,--as-needed -Wl,--no-undefined -shared -fPIC -Wl,--start-group -Wl,-soname,libtsshmm.so /usr/lib/x86_64-linux-gnu/libghmm.so -lm -Wl,--end-group
[8/12] /home/omsai/.local/bin/meson --internal symbolextractor /home/omsai/src/tssghmm/build libtsshmm.so libtsshmm.so libtsshmm.so.p/libtsshmm.so.symbols
[9/12] cc  -o test-init test-init.p/test-init.c.o -Wl,--as-needed -Wl,--no-undefined '-Wl,-rpath,$ORIGIN/' -Wl,-rpath-link,/home/omsai/src/tssghmm/build/ -Wl,--start-group libtsshmm.so /usr/lib/x86_64-linux-gnu/libghmm.so -Wl,--end-group
[10/12] cc  -o test-converge test-converge.p/test-converge.c.o -Wl,--as-needed -Wl,--no-undefined '-Wl,-rpath,$ORIGIN/' -Wl,-rpath-link,/home/omsai/src/tssghmm/build/ -Wl,--start-group libtsshmm.so /usr/lib/x86_64-linux-gnu/libghmm.so -Wl,--end-group
[11/12] cc  -o test-monotonic test-monotonic.p/test-monotonic.c.o -Wl,--as-needed -Wl,--no-undefined '-Wl,-rpath,$ORIGIN/' -Wl,-rpath-link,/home/omsai/src/tssghmm/build/ -Wl,--start-group libtsshmm.so /usr/lib/x86_64-linux-gnu/libghmm.so -Wl,--end-group
[12/12] cc  -o test-stable test-stable.p/test-stable.c.o -Wl,--as-needed -Wl,--no-undefined '-Wl,-rpath,$ORIGIN/' -Wl,-rpath-link,/home/omsai/src/tssghmm/build/ -Wl,--start-group libtsshmm.so /usr/lib/x86_64-linux-gnu/libghmm.so -Wl,--end-group
$ ninja -C build/ test
ninja: Entering directory `build/'
[0/1] Running all tests.
1/4 tssghmm:smoke / tss model initializes and prints        OK              0.02s
2/4 tssghmm:train / tss training is monotonic               OK              0.02s
3/4 tssghmm:train / tss training is stable                  OK              0.03s
4/4 tssghmm:train / tss training converges                  OK              0.07s


Ok:                 4
Expected Fail:      0
Fail:               0
Unexpected Pass:    0
Skipped:            0
Timeout:            0

Full log written to /home/omsai/src/tssghmm/build/meson-logs/testlog.txt
```

# Skipping slow tests

Some tests take the better part of an hour and use a lot of memory for
simulations.  The slow test suite can be skipped with meson instead of ninja:

```console
$ cd build/
$ meson test --no-suite slow
```

# Build the GHMM dependency from source

For debugging purposes, it's useful to build the GHMM library with
debugging symbols using `CFLAGS=-g`.  You can do this with the
following configure command:

```console
$ cd /tmp
$ wget https://sourceforge.net/projects/ghmm/files/ghmm/ghmm%200.9-rc3/ghmm-0.9-rc3.tar.gz/download
...
$ tar -xf ghmm-0.9-rc3.tar.gz
$ mkdir build-ghmm
$ cd build-ghmm/
$ ../ghmm-0.9-rc3/configure \
     CFLAGS=-g \
	 --disable-gsl \
	 --without-python \
	 --prefix=/path/to/install
```

... of course, replace the `/path/to/install` with the location you
want to put the library; I personally use `$HOME/.apps/ghmm`.

Running GHMM requires libxml2 so on Debian you also need `libxml2-dev`
installed.  It turns out using the optional dependency on GSL causes
runtime errors with recent GSL releases because GHMM was originally
written against GSL versions only up to 0.7 whereas the current Debian
version is 2.5.

Note that many tests use GSL, and there's a build system bug of the
tests always built and involked even when installing or cleaning the
package.  As a workaround, one can delete the test files and replace
any offending Makefile rules you might come across with no-op rules:

```console
$ rm -r tests/
$ mkdir tests/
$ cat > tests/Makefile <<EOF
.PHONY : noop all install clean clean-recursive
noop :
	@echo Ignoring $@
all : default
install : default
clean : default
clean-recursive : default
EOF
```

Now that the offending tests have been removed, one can build and
install as usual:

```console
$ make -j install
```

# Build TSS GHMM using your compiled GHMM

Modify the `ghmm.pc` file to switch the `prefix` and `libdir`
variables to use your install prefix above.  I installed to
`$HOME/.apps/ghmm`, so you would need to change your prefix to the
location you used in your `configure ... --prefix=` command above.

```diff
diff --git a/ghmm.pc b/ghmm.pc
index 23333a7..9c02913 100644
--- a/ghmm.pc
+++ b/ghmm.pc
@@ -1,8 +1,8 @@
-prefix=/usr
-#prefix=/home/omsai/.apps/ghmm
+#prefix=/usr
+prefix=/home/omsai/.apps/ghmm
 includedir=${prefix}/include
-libdir=${prefix}/lib/x86_64-linux-gnu
-#libdir=${prefix}/lib
+#libdir=${prefix}/lib/x86_64-linux-gnu
+libdir=${prefix}/lib
 
 Name: GHMM
 Description: General Hidden Markov Model library (GHMM)
```

For some reason, I the system library steals precedence over my
compiled GHMM inspite of the RPATHs so I unfortunately have to
uninstall it:

```console
$ sudo apt remove libgmm-dev
$ sudo apt autoremove
```

Then rerun your `meson` and `ninja` commands as before against your
newly modified `ghmm.pc` file:

```console
$ cd ~/src/tssghmm/
$ rm -r build/
$ meson --pkg-config-path $PWD build
$ ninja -v -C build/
$ ninja -C build/ test
```

Now when you run `gdb` or `valgrind` you will have more access to
seeing the GHMM sources.

# Build the documentation

Initialize the submodule for the nicer CSS files, then generate the
HTML and man documentation.

```console
$ git submodule init
$ git submodule update
$ doxygen Doxyfile
$ man doc/man/man3/tssghmm.h.3
$ xdg-open doc/html/index.html
```
