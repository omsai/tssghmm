/** @file

    @brief Compare models.
  */

#include <math.h>

#include <ghmm/model.h>

/** Print 2 models and their probability differences.

    Print the transition and emission probabilities with columns of
    probabilites from the first model, probabilities from the second
    model, difference in probabilities of the second from the first.
    Track the the 2 maximum difference for transition and emission
    matrices.

    @param prob_max Output array to store the maximum absolute
                    differences between the transition and emission
                    matrices, respectively.

    @param models Input array of 2 models, with the second model being
                  the reference that the first model is compared to.
 */
void
print_and_compare(double prob_max[2], ghmm_dmodel* models[2])
{
  for (int p = 0; p < 2; ++p)
    prob_max[p] = 0.0;
  double diff, prob[2];

  /* Compare transition probabilities. */
  fprintf(stdout, "\ntransition probabilities:\n");
  fprintf(stdout, "i j   actual    expected  diff\n");
  for (int i = 0; i < models[1]->N; ++i)
    for (int j = 0; j < models[1]->N; ++j) {
      if (ghmm_dmodel_check_transition(models[1], i, j)) {
	fprintf(stdout, "%d %d : ", i, j);
	for (int m = 0; m < 2; ++m)
	  fprintf(stdout, "%f  ",
		  prob[m] = ghmm_dmodel_get_transition(models[m], i, j));
	diff = prob[0] - prob[1];
	if (fabs(diff) > prob_max[0])
	  prob_max[0] = fabs(diff);
	fprintf(stdout, "%+f\n", diff);
      }
    }
  fprintf(stdout, "           max(abs(diff))  %f\n", prob_max[0]);

  /* Compare emission probabilities. */
  fprintf(stdout, "\nemission probabilities:\n");
  fprintf(stdout, "i j   actual    expected  diff\n");
  for (int i = 0; i < models[1]->N; ++i)
    for (int j = 0; j < models[1]->M; ++j) {
      fprintf(stdout, "%d %d : ", i, j);
      for (int m = 0; m < 2; ++m)
	fprintf(stdout, "%f  ", prob[m] = models[m]->s[i].b[j]);
      diff = prob[0] - prob[1];
      if (fabs(diff) > prob_max[1])
	prob_max[1] = fabs(diff);
      fprintf(stdout, "%+f\n", diff);
    }
  fprintf(stdout, "           max(abs(diff))  %f\n", prob_max[1]);
}

/** Calculate L2-norm separately of transition and emission matrices.

    The R package HMM on CRAN calculates the L2-norm during training.
    This seems like a another useful way of tracking convergence with
    other alternatives being log-likelihood comparisons or other
    relative tolerance checks.

    @param l2_norms Output array to store L2-norms of transition and
                    emission matrices, respectively.

    @param models Input array of the 2 models to compare; order does
                  not matter.
  */
void
l2_norm(double l2_norms[2], ghmm_dmodel* models[2])
{
  enum {TRANS, EMIS, N};
  for (int p = TRANS; p < N; ++p)
    l2_norms[p] = 0.0;

  for (int i = 0; i < models[1]->N; ++i) {
    double prob[2];
    /* Sum squares of transitions. */
    for (int j = 0; j < models[1]->N; ++j)
      if (ghmm_dmodel_check_transition(models[1], i, j)) {
	for (int m = 0; m < 2; ++m)
	  prob[m] = ghmm_dmodel_get_transition(models[m], i, j);
	l2_norms[TRANS] += pow(prob[0] - prob[1], 2);
      }

    /* Sum squares of emissions. */
    for (int j = 0; j < models[1]->M; ++j) {
	for (int m = 0; m < 2; ++m)
	  prob[m] = models[m]->s[i].b[j];
	l2_norms[EMIS] += pow(prob[0] - prob[1], 2);
    }
  }
  /* Square root of sume of squares. */
  for (int p = TRANS; p < N; ++p)
    l2_norms[p] = sqrt(l2_norms[p]);
}
