/** @file

    @brief Primary API header.
 */

#ifndef TSSGHMM_H
#define TSSGHMM_H

#include <ghmm/model.h>

void model_tsshmm(ghmm_dmodel** model);

void print_and_compare(double prob_max[2], ghmm_dmodel* models[2]);
void l2_norm(double l2_norms[2], ghmm_dmodel* models[2]);

#endif	/* TSSGHMM_H */
