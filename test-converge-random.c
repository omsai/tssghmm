/** @file

    @brief Does the model converge starting from fully random initial values?

    Check convergence training.  Using fully random values in this test almost
    always converges to a local minimum instead of the generated data,
    therefore the only sensible assertion is for convergence and not any
    assertions of error tolerance on the final transition or emission values.
 */

#include <assert.h>
#include <stdlib.h>

#include <ghmm/reestimate.h>

#include "tssghmm.h"

int
main()
{
  /* Enable debug logging. */
  ghmm_set_loglevel(4);		/* LDEBUG */

  /* Initialize model. */
  ghmm_dmodel* expected = NULL;
  model_tsshmm(&expected);

  /* Copy model and completely randomize all parameters. */
  ghmm_dmodel* model = ghmm_dmodel_copy(expected);
  srand(12345);
  /* Fully randomize transitions. */
  for (int i = 0; i < model->N; ++i) {
    ghmm_dstate state = model->s[i];
    /* Each row adds up to 1, so only randomize up to the penultimate
       row parameter. */
    double remaining = 1.0;
    int j = 0;
    for (; j < state.out_states - 1; ++j) {
      double prob = remaining * ((double)rand()) / RAND_MAX;
      ghmm_dmodel_set_transition(model, i, state.out_id[j], prob);
      remaining -= prob;
    }
    ghmm_dmodel_set_transition(model, i, state.out_id[j], remaining);
  }
  /* Fully randomize emissions. */
  for (int i = 0; i < model->N; ++i) {
    /* Only randomize non-tied states. */
    if (model->tied_to[i] == GHMM_kUntied ||
  	model->tied_to[i] == i) {
      ghmm_dstate state = model->s[i];
      /* Each row adds up to 1, so only randomize up to the penultimate
  	 row parameter. */
      double remaining = 1.0;
      int j = 0;
      const int N_EMIS = 3;
      for (; j < N_EMIS - 1; ++j) {
  	double prob = remaining * ((double)rand()) / RAND_MAX;
  	state.b[j] = prob;
  	remaining -= prob;
      }
      state.b[j] = remaining;
    }
  }

  /* Print probability matrices before training. */
  fprintf(stdout, "\nBefore training:\n");
  ghmm_dmodel_print(stdout, model);
  /* Flush in case the next model sanity test or sequence malloc fails. */
  fflush(stdout);

  /* Sanity check randomized parameters. */
  assert(ghmm_dmodel_check(model) == 0);

  /* Verify the changed model trained on data from the true model
     converges to the true parameters. */
  ghmm_dseq* seq =
    ghmm_dmodel_generate_sequences(expected, 12345, 10000, 1, 1);
  int converged = ghmm_dmodel_baum_welch(model, seq);
  ghmm_dseq_free(&seq);

  /* Print probability matrices after training. */
  fprintf(stdout, "\nAfter training:\n");
  ghmm_dmodel_print(stdout, model);

  /* Compare transition matrices. */
  double diff_max[2] = {0.0, 0.0};
  ghmm_dmodel* models[2] = {model, expected};
  print_and_compare(diff_max, models);
  /* Flush in case the next convergence assertion fails. */
  fflush(stdout);

  /* Should converge. */
  assert(converged == 0);

  /* Cleanup models. */
  ghmm_dmodel_free(&model);
  ghmm_dmodel_free(&expected);

  return 0;
}
