/** @file

    @brief Does the model converge with extremely long generated sequences?
 */

#include <assert.h>
#include <stdlib.h>

#include <ghmm/reestimate.h>

#include "tssghmm.h"

int
main()
{
  /* Enable debug logging. */
  ghmm_set_loglevel(4);		/* LDEBUG */

  /* Initialize model. */
  ghmm_dmodel* expected = NULL;
  model_tsshmm(&expected);

  /* Copy model and completely randomize all parameters. */
  ghmm_dmodel* model = ghmm_dmodel_copy(expected);
  srand(12345);
  /* Offset transitions. */
  ghmm_dmodel_set_transition(model, 0, 0, 0.8);
  ghmm_dmodel_set_transition(model, 0, 1, 0.1);
  ghmm_dmodel_set_transition(model, 0, 4, 0.1);

  /* Print probability matrices before training. */
  fprintf(stdout, "\nBefore training:\n");
  ghmm_dmodel_print(stdout, model);
  /* Flush in case the next model sanity test or sequence malloc fails. */
  fflush(stdout);

  /* Sanity check randomized parameters. */
  assert(ghmm_dmodel_check(model) == 0);

  /* Verify the changed model trained on data from the true model
     converges to the true parameters. */
  ghmm_dseq* seq =		/* global_len -------v    v------ seq_number */
    ghmm_dmodel_generate_sequences(expected, 12345, 34e6, 1, 1);
  /* There are limits to the global_len and seq_number arguments above of
     ghmm_dmodel_generate_sequences().

     seq_number throws SIGSEGV after 1.5e6, but this is an intentional
     limitation of ghmm_dseq_calloc:

       ERROR: sequence.c:ghmm_dseq_calloc(559):
       Number of sequences 1600000 exceeds possible range 1500000

     Setting the integer, global_len upto 33.5e6 works but going a little
     higher to 33.6e6 always fails with SIGSEGV.  Sure, one can increase
     seq_number to effectively exceeding this 33.5e6 global_len limit.  However
     INT_MAX in <limits.h> is 2.1 billion so not sure why 33.6 million fails.
     From gdb:

       $ meson test --suite slow --gdb
       ...
       (gdb) run
       ...
       Program received signal SIGSEGV, Segmentation fault.
       0x00007ffff7eca1f0 in ighmm_cmatrix_stat_alloc ()
           from /lib/x86_64-linux-gnu/libghmm.so.1
       (gdb) bt
       #0  0x00007ffff7eca1f0 in ighmm_cmatrix_stat_alloc ()
           from /lib/x86_64-linux-gnu/libghmm.so.1
       #1  0x00007ffff7edee60 in ighmm_reestimate_alloc_matvek ()
           from /lib/x86_64-linux-gnu/libghmm.so.1
       #2  0x00007ffff7edfb75 in ghmm_dmodel_baum_welch_nstep ()
           from /lib/x86_64-linux-gnu/libghmm.so.1
       #3  0x000055555555535b in main () at ../test-converge-limit.c:54
       (gdb)
 */
  int converged = ghmm_dmodel_baum_welch(model, seq);
  ghmm_dseq_free(&seq);

  /* Print probability matrices after training. */
  fprintf(stdout, "\nAfter training:\n");
  ghmm_dmodel_print(stdout, model);

  /* Compare transition matrices. */
  double diff_max[2] = {0.0, 0.0};
  ghmm_dmodel* models[2] = {model, expected};
  print_and_compare(diff_max, models);
  /* Flush in case the next convergence assertion fails. */
  fflush(stdout);

  /* Should converge. */
  assert(converged == 0);

  /* Cleanup models. */
  ghmm_dmodel_free(&model);
  ghmm_dmodel_free(&expected);

  return 0;
}
