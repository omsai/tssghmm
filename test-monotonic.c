/** @file

    @brief Do parameters monotonically converge?

    It's not clear why the L2-norms are not monotonic.
*/

#include <assert.h>

#include <ghmm/reestimate.h>

#include "tssghmm.h"

/** Free the previous model and copy the current model as the "new"
    previous model.

    This inefficient set of memory allocations and deallocations is
    unfortunately necessary because there introspection of function
    parameters during training is limited and there are no callback
    functions.  This inefficiency is acceptable for tests, and in
    practise, we would not introspect model state during training; the
    GHMM model itself is open, but the training process, by design, is
    not as open.
*/
void
reset_prev(ghmm_dmodel* models[2])
{
  ghmm_dmodel_free(&models[1]);
  models[1] = ghmm_dmodel_copy(models[0]);
}

int
main()
{
  /* Enable debug logging. */
  ghmm_set_loglevel(4);		/* LDEBUG */

  /* Initialize model. */
  ghmm_dmodel* model = NULL;
  model_tsshmm(&model);
  ghmm_dmodel* model_offset = ghmm_dmodel_copy(model);
  ghmm_dmodel_set_transition(model_offset, 0, 0, 0.9);
  ghmm_dmodel_set_transition(model_offset, 0, 1, 0.05);
  ghmm_dmodel_set_transition(model_offset, 0, 4, 0.05);
  assert(ghmm_dmodel_check(model_offset) == 0);

  /* Verify l2-norm is monotonic. */
  ghmm_dseq* seq =
    ghmm_dmodel_generate_sequences(model_offset, 12345, 50, 1, 1);
  const double tol = 1e-5;
  ghmm_dmodel* model_prev = ghmm_dmodel_copy(model);
  ghmm_dmodel_baum_welch_nstep(model, seq, 1, tol);
  ghmm_dmodel* models[2] = {model, model_prev};
  double l2_norms_prev[2];
  l2_norm(l2_norms_prev, models);
  reset_prev(models);
  fprintf(stdout, "    l2-norms\n");
  fprintf(stdout, " i  trans     emis       "
	  "d(trans)   d(emis)      sum  converged\n");
  int i = 0;
    fprintf(stdout, "%2d  %f  %f\n", i,
	    l2_norms_prev[0], l2_norms_prev[1]);
  for (i = 1; i < 100; ++i) {
    int converged = ghmm_dmodel_baum_welch_nstep(model, seq, 1, tol);
    double l2_norms[2];
    l2_norm(l2_norms, models);
    fprintf(stdout, "%2d  %f  %f  %+f  %+f  %+f  %d\n", i,
	    l2_norms[0], l2_norms[1],
	    l2_norms[0] - l2_norms_prev[0],
	    l2_norms[1] - l2_norms_prev[1],
	    l2_norms[0] - l2_norms_prev[0] +
	    l2_norms[1] - l2_norms_prev[1],
	    converged);
    /* Swap values. */
    reset_prev(models);
    l2_norms_prev[0] = l2_norms[0];
    l2_norms_prev[1] = l2_norms[1];
  }

  /* Cleanup the data and models. */
  ghmm_dseq_free(&seq);
  ghmm_dmodel_free(&models[0]);
  ghmm_dmodel_free(&models[1]);
  ghmm_dmodel_free(&model_offset);

  return 0;
}
