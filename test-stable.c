/** @file

    @brief Does the model remain at the optimal parameters?

    Interestingly, the model parameters worsen but it does report an
    error.
*/

#include <assert.h>

#include <ghmm/reestimate.h>

#include "tssghmm.h"

int
main()
{
  /* Enable debug logging. */
  ghmm_set_loglevel(4);		/* LDEBUG */

  /* Initialize model. */
  ghmm_dmodel* expected = NULL;
  model_tsshmm(&expected);
  ghmm_dmodel* model = ghmm_dmodel_copy(expected);

  /* Print probability matrices before training. */
  fprintf(stdout, "\nBefore training:\n");
  ghmm_dmodel_print(stdout, model);

  /* Check that training the model on simulated data */
  ghmm_dseq* seq =
    ghmm_dmodel_generate_sequences(expected, 12345, 10000, 1, 1);
  int converged = ghmm_dmodel_baum_welch(model, seq);
  ghmm_dseq_free(&seq);
  /* Should fail to converge because it's already at the optimal value. */
  assert(converged == -1);

  /* Print probability matrices after training. */
  fprintf(stdout, "\nAfter training:\n");
  ghmm_dmodel_print(stdout, model);

  /* Compare transition matrices. */
  double diff_max[2] = {0.0, 0.0};
  ghmm_dmodel* models[2] = {model, expected};
  print_and_compare(diff_max, models);

  /* Cleanup the model. */
  ghmm_dmodel_free(&model);
  ghmm_dmodel_free(&expected);

  return 0;
}
