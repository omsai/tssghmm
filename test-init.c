/** @filexs

    @brief Does the model initialize?

    Smoke test to make sure the model initializes. The assertion is
    done in the model initialization function itself, so there is no
    need to repeat any assertions here.  The model matrices are
    printed out for manual validation because the model initialization
    can succeed but still be incorrect: one reason for printing the
    matrices is the corresponding output states for every state must
    be in ascending order!  Otherwise one will see zero values in the
    probability matrix.  This seems like a bug with the GMM library
    because one would expect that the GHMM model initialization should
    probably check for ascending order to avoid this problem.
    Nevertheless we do not check for this strange condition here
    because the other model tests cover this initialization order
    oddity.
 */

#include <ghmm/ghmm.h>

#include "tssghmm.h"

int
main()
{
  /* Enable debug logging. */
  ghmm_set_loglevel(4);		/* LDEBUG */

  /* Initialize model. */
  ghmm_dmodel* model = NULL;
  model_tsshmm(&model);

  /* Print probability matrices. */
  fprintf(stdout, "Transition matrix: \n");
  ghmm_dmodel_A_print(stdout, model, "", " ", "\n");
  fprintf(stdout, "Emission matrix: \n");
  ghmm_dmodel_B_print(stdout, model, "", " ", "\n");

  /* Cleanup the model. */
  ghmm_dmodel_free(&model);

  return 0;
}
