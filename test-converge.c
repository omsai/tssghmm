/** @file

    @brief Does the model converge to a generated sequence?

    Check transition and emission tolerance after training.  Viterbi
    inference on this somewhat complex model is only about 93%, so
    it's not surprising to have to set 7% and 5% error margins on the
    trained transition and emission matrices.
 */

#include <assert.h>

#include <ghmm/reestimate.h>

#include "tssghmm.h"

int
main()
{
  /* Enable debug logging. */
  ghmm_set_loglevel(4);		/* LDEBUG */

  /* Initialize model. */
  ghmm_dmodel* expected = NULL;
  model_tsshmm(&expected);

  /* Copy model and change parameters. */
  ghmm_dmodel* model = ghmm_dmodel_copy(expected);
  ghmm_dmodel_set_transition(model, 0, 0, 0.9);
  ghmm_dmodel_set_transition(model, 0, 1, 0.05);
  ghmm_dmodel_set_transition(model, 0, 4, 0.05);
  assert(ghmm_dmodel_check(model) == 0);

  /* Print probability matrices before training. */
  fprintf(stdout, "\nBefore training:\n");
  ghmm_dmodel_print(stdout, model);

  /* Verify the changed model trained on data from the true model
     converges to the true parameters. */
  ghmm_dseq* seq =
    ghmm_dmodel_generate_sequences(expected, 12345, 10000, 1, 1);
  int converged = ghmm_dmodel_baum_welch(model, seq);
  ghmm_dseq_free(&seq);
  /* Should converge. */
  assert(converged == 0);

  /* Print probability matrices after training. */
  fprintf(stdout, "\nAfter training:\n");
  ghmm_dmodel_print(stdout, model);

  /* Compare transition matrices. */
  double diff_max[2] = {0.0, 0.0};
  ghmm_dmodel* models[2] = {model, expected};
  print_and_compare(diff_max, models);
  enum {TRANS, EMIS};
  assert(diff_max[TRANS] < 0.07);
  assert(diff_max[EMIS] < 0.07);

  /* Cleanup models. */
  ghmm_dmodel_free(&model);
  ghmm_dmodel_free(&expected);

  return 0;
}
